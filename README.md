# Docker for Tensorflow + Nuxt + Flask + Redis + RQ

This repo is a M.W.E. of a common development paradigm I find myself in, namely
having to develop:

1. a library / module to support a TF model,
2. a tf model which can be served,
2. an api to feature / make available the traind model, and
3. a user friendly front end to utilize the backend.

In this repository:

1. is a custom python library `pyapp`,
2. is a simple task-based flask app (`backend`),
3. is a nuxt app (`frontend`), and
4. is a notebook for making  toy model (`notebooks`).

As this is a _minimal_ example, here `pyapp` does very little. It simply sets
the flask apps name. The full inter-connectivey of the app is as follows.

The nuxt frontend (built with vue, vuetify, axios, etc) makes requests to
the flask backend. The request structure is two fold. First is to enqueue the task,
which is handled via a redis server and rq, and the second is to check the status of the
task until complete. Normally the task in question in some function or wrapper
over a series of functions from `pyapp`. In this case it is the "trained" toy model.



**DISCLAIMER**: I am not a docker guru. Utilizes this at your own peril.

If I did this right, in theory:

```bash
# for training the model
docker-compose -f docker-compose.ai.development.yml build
docker-compose -f docker-compose.ai.development.yml up

# for serving the model as a web app
docker-compose -f docker-compose.web.development.yml build
# for training the model
docker-compose -f docker-compose.web.development.yml up
```

should work
